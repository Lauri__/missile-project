﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCannon : MonoBehaviour
{

    public Transform targetToHit;
    public Transform spawnPosition;

    private float coolDownTime;
    private float coolDownTimerMax = 0.65f;

    void Start()
    {
        coolDownTime = 0f;
    }

    void Update()
    {
        //Shoot
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (coolDownTime <= 0.0f)
            {
                coolDownTime = coolDownTimerMax;
                shoot();
            }
        }
        coolDownTime -= Time.deltaTime;
    }


    void shoot()
    {
        if (targetToHit != null)
        {
            GameObject newMissile = MissileObjectPool.current.GetPooledMissileObject();

            if (newMissile == null)
            {
                return;
            }
            Rigidbody rb = newMissile.GetComponent<Rigidbody>();
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
            newMissile.transform.position = spawnPosition.position;
            newMissile.transform.rotation = spawnPosition.rotation;
            newMissile.SetActive(true);
            Missile mis = newMissile.GetComponent<Missile>() as Missile;
            mis.setTarget(targetToHit);
        }
    }


}

