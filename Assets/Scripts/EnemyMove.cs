﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove : MonoBehaviour {

    private float maxValue = 15;
    private float minValue = -15;
    private float currentValue = 0;
    private float direction = 10;
    private float startY;
    private float startZ;

    void Start()
    {
        startY = transform.position.y;
        startZ = transform.position.z;
    }
    void Update()
    {
        currentValue += Time.deltaTime * direction; 
        if (currentValue >= maxValue)
        {
            direction *= -1;
            currentValue = maxValue;
        }
        else if (currentValue <= minValue)
        {
            direction *= -1;
            currentValue = minValue;
        }
        transform.position = new Vector3(currentValue, startY, startZ);
    }
}
