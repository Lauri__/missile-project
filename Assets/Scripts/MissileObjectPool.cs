﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MissileObjectPool : MonoBehaviour
{

    public static MissileObjectPool current;

    public GameObject missile;

    private int missileCount = 20;
    public List<GameObject> missiles = new List<GameObject>();



    void Awake()
    {
        current = this;
    }

    void Start()
    {
        initMissilePool();

    }


    private void initMissilePool()
    {
        for (int i = 0; i < missileCount; i++)
        {
            GameObject newMissile = (GameObject)Instantiate(missile);
            newMissile.SetActive(false);
            missiles.Add(newMissile);
        }
    }



    public GameObject GetPooledMissileObject()
    {
        GameObject targetObject = null;

        for (int i = 0; i < missiles.Count; i++)
        {
            if (!missiles[i].activeInHierarchy)
            {
                //Debug.Log("Object was not active.");
                return missiles[i];

            }
            else
            {

                //Debug.Log("Object was active.");
            }


        }

        return targetObject;
    }



}

