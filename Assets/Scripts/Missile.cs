﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missile : MonoBehaviour
{

    private Transform missileTarget = null;
    private Rigidbody rb;
    private IEnumerator coroutine;
    private float power = 15;
    private float powerFactor;
    private float lifeTimeSeconds = 6f;
    private bool locking = false;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        powerFactor = 0.01f;
    }

    void Update()
    {
        

        if (missileTarget != null)
        {
            if (locking)
            {

                Vector3 lookPos = missileTarget.position - transform.position;
                Quaternion rot = Quaternion.LookRotation(lookPos, transform.up);
                Quaternion rot2 = Quaternion.RotateTowards(transform.rotation, rot, 40 * Time.deltaTime);
                transform.rotation = Quaternion.Slerp(transform.rotation, rot2, 90 * Time.deltaTime);

            }
            else
            {
                rb.velocity = transform.forward.normalized * (power + powerFactor);
            }
        }
    }

    void LateUpdate()
    {
        if (missileTarget != null)
        {
            if (locking)
            {
                rb.velocity = transform.forward.normalized * (power + powerFactor);
            }
            powerFactor += Time.deltaTime * 3.0f;

        }
    }

    void OnTriggerEnter(Collider col)
    {
        missileTarget = null;
        this.gameObject.SetActive(false);
    }

    void OnCollisionEnter(Collision collision)
    {
        foreach (ContactPoint contact in collision.contacts)
        {
            Debug.DrawRay(contact.point, contact.normal, Color.white);
        }
        if (collision.relativeVelocity.magnitude > 16)
        { 
            missileTarget = null;
            this.gameObject.SetActive(false);
        }
    }

    void OnEnable()
    {
        Invoke("SetInActive", lifeTimeSeconds);
        locking = false;
        coroutine = startLocking(0.2f);
        StartCoroutine(coroutine);
    }

    void OnDisable()
    {
        CancelInvoke();
        locking = false;
        powerFactor = 0.01f;
    }

    void SetInActive()
    {
        missileTarget = null;
        this.gameObject.SetActive(false);
    }

    public void setTarget(Transform target)
    {
        missileTarget = target;
    }

    private IEnumerator startLocking(float waitTime)
    {

        yield return new WaitForSeconds(waitTime);
        locking = true;

    }
}
